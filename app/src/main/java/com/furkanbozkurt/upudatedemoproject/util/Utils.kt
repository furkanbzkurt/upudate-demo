package com.furkanbozkurt.upudatedemoproject.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.view.View
import com.furkanbozkurt.upudatedemoproject.R


object Utils {


    fun getStickers(context: Context): ArrayList<Drawable> {
        val mStickers = ArrayList<Drawable>()
        mStickers.add(context.getDrawableExt(R.drawable.astonished_face_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.cry_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.nerd_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.sad_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.slightly_smiling_face_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.smiley_face_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.smiley_face_tightly_closed_eyes_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.smiley_smiling_eyes_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.smiley_with_sweat_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.smirking_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.sunglasses_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.tears_of_joy_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.unamused_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.upside_down_face_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.evil_monkey_emoji)!!)
        mStickers.add(context.getDrawableExt(R.drawable.penguin_emoji)!!)
        return mStickers
    }

    // get bitmap from sticker + main image container
    fun getBitmapFromView(v: View): Bitmap {
        val b = Bitmap.createBitmap(v.width, v.height, Bitmap.Config.ARGB_8888)
        val c = Canvas(b)
        v.draw(c)
        return b
    }

    // reduce bitmap size to avoid out of memory error
    fun resizeBitmapImage(image: Bitmap, maxResolution: Int): Bitmap? {
        var image = image
        if (maxResolution <= 0) return image
        val width = image.width
        val height = image.height
        val ratio =
            if (width >= height) maxResolution.toFloat() / width else maxResolution.toFloat() / height
        val finalWidth = (width.toFloat() * ratio).toInt()
        val finalHeight = (height.toFloat() * ratio).toInt()
        image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true)
        return image
    }
}