package com.furkanbozkurt.upudatedemoproject.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.furkanbozkurt.upudatedemoproject.di.ViewModelKey
import com.furkanbozkurt.upudatedemoproject.ui.camerawithsticker.CameraWithStickerViewModel
import com.furkanbozkurt.upudatedemoproject.util.ViewModelProvidersFactory

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi

//@Suppress("unused")
@ExperimentalCoroutinesApi
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelProvidersFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(CameraWithStickerViewModel::class)
    abstract fun provideCameraWithStickerViewModel(cameraWithStickerViewModel: CameraWithStickerViewModel?): ViewModel?

}
