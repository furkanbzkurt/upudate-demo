package com.furkanbozkurt.upudatedemoproject.di.module

import android.app.Application
import android.content.Context
import com.furkanbozkurt.upudatedemoproject.util.CameraHelper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideAppContext(): Context = app

    @Provides
    @Singleton
    fun provideCameraHelper(): CameraHelper = CameraHelper(app)
}