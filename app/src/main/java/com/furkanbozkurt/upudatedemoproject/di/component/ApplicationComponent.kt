package com.furkanbozkurt.upudatedemoproject.di.component

import com.furkanbozkurt.upudatedemoproject.MainApp
import com.furkanbozkurt.upudatedemoproject.di.module.ActivityBuilder
import com.furkanbozkurt.upudatedemoproject.di.module.AppModule
import com.furkanbozkurt.upudatedemoproject.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [(AndroidInjectionModule::class),
        (AndroidSupportInjectionModule::class),
        (AppModule::class),
        (ActivityBuilder::class),
        (ViewModelModule::class)]
)

interface ApplicationComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(mainApp: MainApp): Builder

        fun appModule(appModule: AppModule): Builder

        fun build(): ApplicationComponent
    }

    fun inject(mainApp: MainApp)
}