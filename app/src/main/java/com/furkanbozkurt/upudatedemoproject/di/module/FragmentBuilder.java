package com.furkanbozkurt.upudatedemoproject.di.module;


import com.furkanbozkurt.upudatedemoproject.ui.camerawithsticker.CameraWithStickerFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract CameraWithStickerFragment contributeCameraWithStickerFragment();

}
