package com.furkanbozkurt.upudatedemoproject.ui.adapters

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.furkanbozkurt.upudatedemoproject.databinding.ItemStickerBinding


class StickerAdapter(private val stickerClickListener: StickerClickListener) :
    ListAdapter<Drawable, RecyclerView.ViewHolder>(StickersDiffCallback()) {

    interface StickerClickListener {
        fun onStickerClick(drawable: Drawable)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return StickerViewHolder(
            ItemStickerBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val record = getItem(position)
        (holder as StickerViewHolder).bind(record, stickerClickListener)
    }

    class StickerViewHolder(
        private val binding: ItemStickerBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(
            itemSticker: Drawable,
            stickerClickListener: StickerClickListener
        ) {
            binding.imageViewSticker.setImageDrawable(itemSticker)
            binding.imageViewSticker.setOnClickListener {
                stickerClickListener.onStickerClick(itemSticker)
            }
        }
    }
}

private class StickersDiffCallback : DiffUtil.ItemCallback<Drawable>() {

    override fun areItemsTheSame(oldItem: Drawable, newItem: Drawable): Boolean {
        return oldItem == newItem
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: Drawable, newItem: Drawable): Boolean {
        return oldItem == newItem
    }
}