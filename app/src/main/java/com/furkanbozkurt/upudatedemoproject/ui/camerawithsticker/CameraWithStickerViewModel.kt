package com.furkanbozkurt.upudatedemoproject.ui.camerawithsticker

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.furkanbozkurt.upudatedemoproject.util.Utils
import com.furkanbozkurt.upudatedemoproject.util.rotate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.IOException
import javax.inject.Inject

class CameraWithStickerViewModel @Inject constructor(
    private val appContext: Context
) : ViewModel() {

    var takePictureStatus = MutableLiveData<Boolean>(false)
    var closePictureStatus = MutableLiveData<Boolean>(false)
    var takePictureFromGalleryStatus = MutableLiveData<Boolean>(false)
    var savePictureStatus = MutableLiveData<Boolean>(false)
    var visibilityStatus = MutableLiveData<Boolean>(false)

    private val _galleryBitmapLiveData = MutableLiveData<Bitmap>()
    val galleryBitmapLiveData: LiveData<Bitmap>
        get() = _galleryBitmapLiveData

    private val _capturedBitmapLiveData = MutableLiveData<Bitmap>()
    val capturedBitmapLiveData: LiveData<Bitmap>
        get() = _capturedBitmapLiveData


    init {
        visibilityStatus.postValue(true)
    }

    fun onTakePictureClick() {
        viewModelScope.launch(Dispatchers.IO) {
            takePictureStatus.postValue(true)
        }
    }

    fun onClosePictureClick() {
        viewModelScope.launch(Dispatchers.IO) {
            closePictureStatus.postValue(true)
            visibilityStatus.postValue(true)
        }
    }

    fun onTakePictureFromGalleryClick() {
        viewModelScope.launch(Dispatchers.IO) {
            takePictureFromGalleryStatus.postValue(true)
        }
    }

    fun onSaveImageClick() {
        viewModelScope.launch(Dispatchers.IO) {
            savePictureStatus.postValue(true)
            visibilityStatus.postValue(true)
        }
    }

    fun processGalleryIntent(data: Intent?) {
        viewModelScope.launch(Dispatchers.IO) {
            val selectedImage: Uri? = data?.data
            try {
                visibilityStatus.postValue(false)
                val bitmap = MediaStore.Images.Media.getBitmap(
                    appContext.contentResolver,
                    selectedImage
                )
                // resize bitmap
                var resized = bitmap?.let {
                    Bitmap.createScaledBitmap(
                        it,
                        ((bitmap.width * 1F).toInt()),
                        ((bitmap.height * 1F).toInt()),
                        true
                    )
                }

                // reduce bitmap size to avoid out of memory error
                resized = resized?.let { Utils.resizeBitmapImage(it, 800) }
                _galleryBitmapLiveData.postValue(resized)

            } catch (e: IOException) {
                Log.i("TAG", "Some exception $e")
            }
        }
    }

    fun processCapturedImage(bitmap: Bitmap?) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                visibilityStatus.postValue(false)
                // resize bitmap
                var resized = bitmap?.let {
                    Bitmap.createScaledBitmap(
                        it,
                        ((bitmap.width * 1F).toInt()),
                        ((bitmap.height * 1F).toInt()),
                        true
                    )
                }

                // reduce bitmap size to avoid out of memory error
                resized = resized?.let { Utils.resizeBitmapImage(it, 800) }

                // captured images are automatically rotating when trying to get bitmap from cameraX
                // (it's because of cameraX library is not stable yet), to solve this issue;
                // I rotated bitmap before setting to view
                _capturedBitmapLiveData.postValue(resized?.rotate(90F))

            } catch (e: IOException) {
                Log.i("TAG", "Some exception $e")
            }
        }
    }

}