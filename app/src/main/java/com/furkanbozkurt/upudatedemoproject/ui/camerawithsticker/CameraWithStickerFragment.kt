package com.furkanbozkurt.upudatedemoproject.ui.camerawithsticker

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.furkanbozkurt.upudatedemoproject.R
import com.furkanbozkurt.upudatedemoproject.databinding.FragmentCameraWithStickerBinding
import com.furkanbozkurt.upudatedemoproject.ui.adapters.StickerAdapter
import com.furkanbozkurt.upudatedemoproject.util.*
import com.furkanbozkurt.upudatedemoproject.util.Constants.Companion.GALLERY_REQUEST
import com.furkanbozkurt.upudatedemoproject.util.Constants.Companion.REQUEST_CODE_PERMISSIONS
import com.furkanbozkurt.upudatedemoproject.util.Constants.Companion.REQUIRED_PERMISSIONS
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject


class CameraWithStickerFragment : Fragment(), CameraHelper.CameraCapturedListener,
    StickerAdapter.StickerClickListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvidersFactory

    @Inject
    lateinit var appContext: Context

    @Inject
    lateinit var cameraHelper: CameraHelper


    private lateinit var viewModel: CameraWithStickerViewModel
    private lateinit var binding: FragmentCameraWithStickerBinding

    private val mAdapter: StickerAdapter by lazy {
        StickerAdapter(
            stickerClickListener = this
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        AndroidSupportInjection.inject(this)
        binding = FragmentCameraWithStickerBinding.inflate(inflater, container, false)

        init()
        checkPermissions()

        return binding.root
    }

    private fun init() {
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(CameraWithStickerViewModel::class.java)
        binding.recyclerViewStickers.adapter = mAdapter
        binding.viewModel = viewModel
        // initialize camera helper to use properties and receive events with callbacks from camera
        cameraHelper.fragment = this
        cameraHelper.cameraCapturedListener = this
    }

    private fun checkPermissions() {
        if (allPermissionsGranted()) {
            cameraHelper.startCamera()
            observeEvents()
        } else {
            requestPermissions(REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
        }
    }

    private fun observeEvents() {
        // taking photo event from camera
        viewModel.takePictureStatus.observe(viewLifecycleOwner, Observer {
            if (it) cameraHelper.takePhoto()
        })

        // cancel photo event
        viewModel.closePictureStatus.observe(viewLifecycleOwner, Observer {
            if (it) {
                cameraHelper.reBindCamera()
                // reset sticker imageview content
                binding.imageViewSticker.setImageDrawable(null)
            }
        })

        // taking photo from device gallery event
        viewModel.takePictureFromGalleryStatus.observe(viewLifecycleOwner, Observer {
            if (it) {
                val photoPickerIntent = Intent(Intent.ACTION_PICK)
                photoPickerIntent.type = "image/*"
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST)
            }
        })

        // process photo from gallery in viewmodel & get bitmap result
        viewModel.galleryBitmapLiveData.observe(viewLifecycleOwner, Observer {
            // setting stickers to recyclerview
            mAdapter.submitList(Utils.getStickers(appContext))
            binding.recyclerViewStickers.show()
            binding.imageViewCapturedImage.setImageBitmap(it)
        })

        // saving edited photo to device gallery
        viewModel.savePictureStatus.observe(viewLifecycleOwner, Observer {
            if (it) {
                cameraHelper.saveImage(
                    Utils.getBitmapFromView(binding.relativeLayoutImageContainer),
                    appContext,
                    getString(R.string.app_name)
                )
                cameraHelper.reBindCamera()
                // reset sticker imageview content
                binding.imageViewSticker.setImageDrawable(null)
                showToast(getString(R.string.desc_image_saved))
            }
        })

        // process captured image in viewmodel & get bitmap result
        viewModel.capturedBitmapLiveData.observe(viewLifecycleOwner, Observer {
            // setting stickers to recyclerview
            mAdapter.submitList(Utils.getStickers(appContext))
            binding.recyclerViewStickers.show()
            binding.imageViewCapturedImage.setImageBitmap(it)
        })

        // set layout elements visibility by take photo, save photo etc. states
        viewModel.visibilityStatus.observe(viewLifecycleOwner, Observer {
            binding.visibilityState = it
        })
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(appContext, it) == PackageManager.PERMISSION_GRANTED
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                cameraHelper.startCamera()
                observeEvents()
            } else {
                showToast(getString(R.string.desc_permission_denied))
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                GALLERY_REQUEST -> {
                    // process selected image from gallery in viewmodel
                    viewModel.processGalleryIntent(data)
                }
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        // destroy executor service on destroy
        cameraHelper.shutDown()
    }

    // get captured image from camera helper & process it in viewmodel
    override fun onImageCapture(bitmap: Bitmap?) {
        viewModel.processCapturedImage(bitmap)
    }

    // get selected sticker from recylerview adapter and set sticker to view
    override fun onStickerClick(drawable: Drawable) {
        binding.imageViewSticker.setImageDrawable(drawable)
    }

}
