package com.furkanbozkurt.upudatedemoproject

import android.app.Activity
import android.app.Application
import com.furkanbozkurt.upudatedemoproject.di.component.DaggerApplicationComponent
import com.furkanbozkurt.upudatedemoproject.di.module.AppModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class MainApp : Application(), HasActivityInjector {
    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        @Suppress("DEPRECATION")
        DaggerApplicationComponent.builder()
            .application(this)
            .appModule(AppModule(this))
            .build()
            .inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityInjector
    }
}